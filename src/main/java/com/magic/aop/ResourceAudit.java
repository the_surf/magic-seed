package com.magic.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: ResourceAudit <br/>
 * Function: 资源审计. <br/>
 * date: 2018年11月8日 下午4:28:27 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ResourceAudit {

	String value() default "";

	String name() default "";

	String notes() default "";
}
