package com.magic.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.magic.entity.user.Flower;

/**
 * ClassName: IFlowerRepository <br/>
 * Function: 花种. <br/>
 * date: 2018年10月16日 下午5:53:51 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Transactional
public interface IFlowerRepository
		extends PagingAndSortingRepository<Flower, Integer>, JpaSpecificationExecutor<Flower> {

	List<Flower> findByUserId(int userId);
}
