package com.magic.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.magic.entity.gift.Gift;

/**
 * ClassName: IGiftRepository <br/>
 * Function: 礼品. <br/>
 * date: 2018年10月16日 下午7:14:14 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Transactional
public interface IGiftRepository extends PagingAndSortingRepository<Gift, Integer>, JpaSpecificationExecutor<Gift> {

	@Query(nativeQuery = true, value = "SELECT s.* FROM gift s ORDER BY s.createdtime DESC limit ?1,?2")
	List<Gift> pageSearch(int pageNum, int pageSize);
}
