package com.magic.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.magic.entity.pay.TradeForm;

/** 
 * ClassName: ITradeFormRepository <br/> 
 * Function: 交易单. <br/> 
 * date: 2018年10月16日 下午5:29:35 <br/> 
 * 
 * @author lishuai11 
 * @version  
 * @since JDK 1.8
 */
@Transactional
public interface ITradeFormRepository
		extends PagingAndSortingRepository<TradeForm, Integer>, JpaSpecificationExecutor<TradeForm> {

	List<TradeForm> findByUserIdAndTradeStatus(int userId, String tradeStatus);

	List<TradeForm> findByUserIdAndTradeCode(int userId, String tradeCode);
	
	@Query("SELECT t FROM TradeForm t WHERE t.userId=?1 ORDER BY t.createdTime DESC")
	List<TradeForm> findByUserId(int userId);

	List<TradeForm> findByTradeCode(String tradeCode);

	void deleteByTradeCode(String tradeCode);
}
