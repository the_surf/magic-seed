package com.magic.vo;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: ForgetPassword <br/>
 * Function: 忘记密码. <br/>
 * date: 2018年10月16日 上午9:45:06 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
public class ForgetPassword {

	public ForgetPassword() {

	}

	public ForgetPassword(String mobile, String firstPassword, String secondPassword, String code) {
		this.mobile = mobile;
		this.firstPassword = firstPassword;
		this.secondPassword = secondPassword;
		this.code = code;
	}

	@ApiModelProperty(name = "mobile", value = "手机号", dataType = "string", required = true)
	@NotBlank(message = "mobile不为空")
	@Pattern(regexp = "^(((13[0-9])|(14[579])|(15([0-3]|[5-9]))|(16[6])|(17[0135678])|(18[0-9])|(19[89]))\\d{8})$", message = "非法手机号")
	private String mobile;

	@ApiModelProperty(name = "firstPassword", value = "输入密码", dataType = "string", required = true)
	@NotBlank(message = "firstPassword不为空")
	@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[\\s\\S]{8,16}$",message = "密码至少包含一个大写字母,一个小写字母,一个数字,8到16位")
	private String firstPassword;

	@ApiModelProperty(name = "secondPassword", value = "再次输入密码", dataType = "string", required = true)
	@NotBlank(message = "secondPassword不为空")
	@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[\\s\\S]{8,16}$",message = "密码至少包含一个大写字母,一个小写字母,一个数字,8到16位")
	private String secondPassword;

	@ApiModelProperty(name = "code", value = "验证码", dataType = "string", required = true)
	@NotBlank(message = "code不为空")
	private String code;

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the firstPassword
	 */
	public String getFirstPassword() {
		return firstPassword;
	}

	/**
	 * @param firstPassword
	 *            the firstPassword to set
	 */
	public void setFirstPassword(String firstPassword) {
		this.firstPassword = firstPassword;
	}

	/**
	 * @return the secondPassword
	 */
	public String getSecondPassword() {
		return secondPassword;
	}

	/**
	 * @param secondPassword
	 *            the secondPassword to set
	 */
	public void setSecondPassword(String secondPassword) {
		this.secondPassword = secondPassword;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
