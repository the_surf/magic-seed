package com.magic.websocket.push;

import java.math.BigDecimal;

/**
 * ClassName: MessageVo <br/>
 * Function: websocket推送数据. <br/>
 * date: 2018年10月26日 上午10:51:16 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public class MessageVo {

	public MessageVo() {
		this.type = "message";
	}

	public MessageVo(int gameId, String mobile, String nickname, String city, BigDecimal price, BigDecimal needPrice,
			BigDecimal bonus, BigDecimal inputSeeds) {
		this.gameId = gameId;
		this.mobile = mobile;
		if (nickname == null || nickname.equals("")) {
			this.nickname = "无";
		}
		this.nickname = nickname;
		if (city == null || city.equals("")) {
			this.city = "郑州市";
		}
		this.city = city;
		this.price = price;
		this.needPrice = needPrice;
		this.bonus = bonus;
		this.inputSeeds = inputSeeds;
		this.type = "message";
	}

	// 游戏id
	private int gameId;
	// 手机号
	private String mobile;
	// 昵称(用户无昵称默认无)
	private String nickname = "无";
	// 用户城市(用户无城市默认郑州市)
	private String city = "郑州市";
	// 用户出价
	private BigDecimal price;
	// 所需花种
	private BigDecimal needPrice;
	// 游戏奖金池
	private BigDecimal bonus;
	// 游戏投入总花种
	private BigDecimal inputSeeds;
	// 区分信息和倒计时时间
	private String type;

	/**
	 * @return the gameId
	 */
	public int getGameId() {
		return gameId;
	}

	/**
	 * @param gameId
	 *            the gameId to set
	 */
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the cost
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param cost
	 *            the cost to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname
	 *            the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the bonus
	 */
	public BigDecimal getBonus() {
		return bonus;
	}

	/**
	 * @param bonus
	 *            the bonus to set
	 */
	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}

	/**
	 * @return the inputSeeds
	 */
	public BigDecimal getInputSeeds() {
		return inputSeeds;
	}

	/**
	 * @param inputSeeds
	 *            the inputSeeds to set
	 */
	public void setInputSeeds(BigDecimal inputSeeds) {
		this.inputSeeds = inputSeeds;
	}

	/**
	 * @return the needPrice
	 */
	public BigDecimal getNeedPrice() {
		return needPrice;
	}

	/**
	 * @param needPrice
	 *            the needPrice to set
	 */
	public void setNeedPrice(BigDecimal needPrice) {
		this.needPrice = needPrice;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
