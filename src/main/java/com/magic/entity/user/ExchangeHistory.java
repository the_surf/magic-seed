package com.magic.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: ExchangeHistory <br/>
 * Function: 兑换记录. <br/>
 * date: 2018年10月21日 下午1:55:29 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
@Entity
@Table(name = "`EXCHANGE_HISTORY`")
@EntityListeners(AuditingEntityListener.class)
public class ExchangeHistory implements Serializable {

	private static final long serialVersionUID = 5200279942427164735L;

	public ExchangeHistory() {

	}

	public ExchangeHistory(int userId, int addressId, int giftId, String giftName, String giftImage) {
		this.userId = userId;
		this.addressId = addressId;
		this.giftId = giftId;
		this.giftName = giftName;
		this.giftImage = giftImage;
	}

	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "`EXCHANGE_ID`")
	private int exchangeId;

	@ApiModelProperty(name = "userId", value = "用户id", required = true, dataType = "int")
	@NotNull(message = "userId不为空")
	@Column(name = "`USER_ID`", nullable = false)
	private int userId;

	@ApiModelProperty(name = "addressId", value = "地址id", required = true, dataType = "int")
	@NotNull(message = "addressId不为空")
	@Column(name = "`address_ID`", nullable = false)
	private int addressId;

	@ApiModelProperty(name = "giftId", value = "礼物id", required = true, dataType = "int")
	@NotNull(message = "giftId不为空")
	@Column(name = "`GIFT_ID`", nullable = false)
	private int giftId;

	/**
	 * ①待发货②待收货③已完成
	 */
	@ApiModelProperty(hidden = true)
	@Column(name = "`STATUS`", nullable = false)
	private String status;

	@ApiModelProperty(hidden = true)
	@CreatedDate
	@Column(name = "`EXCHANGE_TIME`", updatable = false, columnDefinition = "bigint(20) DEFAULT 0")
	private long exchangeTime;

	@ApiModelProperty(name = "giftName", value = "礼物名称", required = true, dataType = "string")
	@NotBlank(message = "礼物名称不为空")
	@Column(name = "`GIFT_NAME`", nullable = false)
	private String giftName;

	@ApiModelProperty(name = "giftImage", value = "礼物图片", required = true, dataType = "string")
	@NotBlank(message = "礼物图片不为空")
	@Column(name = "`GIFT_Image`", nullable = false)
	private String giftImage;

	/**
	 * @return the exchangeId
	 */
	public int getExchangeId() {
		return exchangeId;
	}

	/**
	 * @param exchangeId
	 *            the exchangeId to set
	 */
	public void setExchangeId(int exchangeId) {
		this.exchangeId = exchangeId;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the addressId
	 */
	public int getAddressId() {
		return addressId;
	}

	/**
	 * @param addressId
	 *            the addressId to set
	 */
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	/**
	 * @return the giftId
	 */
	public int getGiftId() {
		return giftId;
	}

	/**
	 * @param giftId
	 *            the giftId to set
	 */
	public void setGiftId(int giftId) {
		this.giftId = giftId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the exchangeTime
	 */
	public long getExchangeTime() {
		return exchangeTime;
	}

	/**
	 * @param exchangeTime
	 *            the exchangeTime to set
	 */
	public void setExchangeTime(long exchangeTime) {
		this.exchangeTime = exchangeTime;
	}

	/**
	 * @return the giftName
	 */
	public String getGiftName() {
		return giftName;
	}

	/**
	 * @param giftName
	 *            the giftName to set
	 */
	public void setGiftName(String giftName) {
		this.giftName = giftName;
	}

	/**
	 * @return the giftImage
	 */
	public String getGiftImage() {
		return giftImage;
	}

	/**
	 * @param giftImage
	 *            the giftImage to set
	 */
	public void setGiftImage(String giftImage) {
		this.giftImage = giftImage;
	}

}
