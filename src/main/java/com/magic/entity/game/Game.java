package com.magic.entity.game;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.magic.entity.object.EntityObject;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: Game <br/>
 * Function: 游戏. <br/>
 * date: 2018年10月21日 下午3:27:56 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
@Entity
@Table(name = "`GAME`")
@EntityListeners(AuditingEntityListener.class)
public class Game extends EntityObject {

	private static final long serialVersionUID = -417752609908903531L;

	public Game() {
		super();
	}

	@ApiModelProperty(hidden = true)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "`GAME_ID`")
	private int gameId;

	@ApiModelProperty(name = "name", value = "游戏名称", required = true, dataType = "string")
	@NotBlank(message = "游戏名称不为空")
	@Column(name = "`NAME`", nullable = false)
	private String name;

	@ApiModelProperty(name = "detail", value = "游戏描述", required = true, dataType = "string")
	@NotBlank(message = "游戏描述不为空")
	@Column(name = "`DETAIL`", nullable = false)
	private String detail;

	@ApiModelProperty(name = "image", value = "游戏图片", required = true, dataType = "string")
	@NotBlank(message = "游戏图片不为空")
	@Column(name = "`IMAGE`", nullable = false)
	private String image;

	@ApiModelProperty(name = "handleTime", value = "场次执行时间(单位分钟,example:1小时等于60分钟)", dataType = "int")
	@Column(name = "`HANDLE_TIME`")
	private int handleTime;

	@ApiModelProperty(name = "gamer", value = "参与人数", dataType = "int", hidden = true)
	@Column(name = "`GAMER`")
	private int gamer;

	@ApiModelProperty(name = "bonus", value = "奖金", dataType = "double", hidden = true)
	@Column(name = "`BONUS`", precision = 12, scale = 2, nullable = false)
	private BigDecimal bonus = new BigDecimal(0.00);

	@ApiModelProperty(name = "inputSeeds", value = "游戏投入总花种", dataType = "double", hidden = true)
	@Column(name = "`INPUT_SEEDS`", precision = 12, scale = 2, nullable = false)
	private BigDecimal inputSeeds = new BigDecimal(0.00);

	@ApiModelProperty(name = "needPrice", value = "所需花种", dataType = "double", hidden = true)
	@Column(name = "`NEED_PRICE`", precision = 12, scale = 2, nullable = false)
	private BigDecimal needPrice = new BigDecimal(0.10);

	@ApiModelProperty(name = "end", value = "游戏是否结束", dataType = "Boolean")
	@Column(name = "`END`", nullable = false)
	private Boolean end = false;

	/**
	 * @return the gameId
	 */
	public int getGameId() {
		return gameId;
	}

	/**
	 * @param gameId
	 *            the gameId to set
	 */
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the detail
	 */
	public String getDetail() {
		return detail;
	}

	/**
	 * @param detail
	 *            the detail to set
	 */
	public void setDetail(String detail) {
		this.detail = detail;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image
	 *            the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the handleTime
	 */
	public int getHandleTime() {
		return handleTime;
	}

	/**
	 * @param handleTime
	 *            the handleTime to set
	 */
	public void setHandleTime(int handleTime) {
		this.handleTime = handleTime;
	}

	/**
	 * @return the gamer
	 */
	public int getGamer() {
		return gamer;
	}

	/**
	 * @param gamer
	 *            the gamer to set
	 */
	public void setGamer(int gamer) {
		this.gamer = gamer;
	}

	/**
	 * @return the bonus
	 */
	public BigDecimal getBonus() {
		return bonus;
	}

	/**
	 * @param bonus
	 *            the bonus to set
	 */
	public void setBonus(BigDecimal bonus) {
		this.bonus = bonus;
	}

	/**
	 * @return the inputSeeds
	 */
	public BigDecimal getInputSeeds() {
		return inputSeeds;
	}

	/**
	 * @param inputSeeds
	 *            the inputSeeds to set
	 */
	public void setInputSeeds(BigDecimal inputSeeds) {
		this.inputSeeds = inputSeeds;
	}

	/**
	 * @return the needPrice
	 */
	public BigDecimal getNeedPrice() {
		return needPrice;
	}

	/**
	 * @param needPrice
	 *            the needPrice to set
	 */
	public void setNeedPrice(BigDecimal needPrice) {
		this.needPrice = needPrice;
	}

	/**
	 * @return the end
	 */
	public Boolean getEnd() {
		return end;
	}

	/**
	 * @param end
	 *            the end to set
	 */
	public void setEnd(Boolean end) {
		this.end = end;
	}

}
