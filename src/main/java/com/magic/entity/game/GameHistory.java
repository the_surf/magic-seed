package com.magic.entity.game;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ClassName: GameHistory <br/>
 * Function: 游戏记录. <br/>
 * date: 2018年10月24日 下午3:46:40 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@ApiModel
@Entity
@Table(name = "`GAME_HISTORY`")
@EntityListeners(AuditingEntityListener.class)
public class GameHistory implements Serializable {

	private static final long serialVersionUID = -3049887858825498946L;

	public GameHistory() {

	}

	public GameHistory(int gameId, int userId, String mobile, String nickName, String city, BigDecimal price) {
		this.gameId = gameId;
		this.userId = userId;
		this.mobile = mobile;
		this.nickName = nickName;
		this.city = city;
		this.price = price;
	}

	@ApiModelProperty(name = "id", value = "主键", dataType = "int")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "`ID`")
	private int id;

	@ApiModelProperty(name = "gameId", value = "游戏id", dataType = "int")
	@Column(name = "`GAME_ID`", nullable = false, updatable = false)
	private int gameId;

	@ApiModelProperty(name = "userId", value = "用户id", dataType = "int")
	@Column(name = "`USER_ID`", nullable = false, updatable = false)
	private int userId;

	@ApiModelProperty(name = "mobile", value = "手机号", dataType = "String")
	@Column(name = "`MOBILE`", nullable = false)
	private String mobile;

	@ApiModelProperty(name = "nickName", value = "昵称", dataType = "String")
	@Column(name = "`NICKNAME`")
	private String nickName;

	@ApiModelProperty(name = "city", value = "市区", dataType = "String")
	@Column(name = "`CITY`")
	private String city;

	@ApiModelProperty(name = "price", value = "出价", dataType = "double")
	@Column(name = "`PRICE`", precision = 12, scale = 2, nullable = false)
	private BigDecimal price = new BigDecimal(0.00);

	@ApiModelProperty(name = "time", value = "创建时间")
	@CreatedDate
	@Column(name = "`TIME`", updatable = false, columnDefinition = "bigint(20) DEFAULT 0")
	private long time;

	/**
	 * @return the historyId
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the gameId
	 */
	public int getGameId() {
		return gameId;
	}

	/**
	 * @param gameId
	 *            the gameId to set
	 */
	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the nickName
	 */
	public String getNickName() {
		return nickName;
	}

	/**
	 * @param nickName
	 *            the nickName to set
	 */
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
