package com.magic.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * ClassName: ValidUtil <br/>
 * date: 2018年9月5日 下午6:00:11 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public class ValidUtil {

	private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();

	public static <T> List<String> validate(T t) {
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<T>> constraintViolations = validator.validate(t);
		List<String> messageList = new ArrayList<>();
		for (ConstraintViolation<T> constraintViolation : constraintViolations) {
			messageList.add(constraintViolation.getMessage());
		}
		return messageList;
	}
	
	/**
	 * 
	 * requestIsValid:(json请求参数是否合法). <br/>
	 * 
	 * @author lishuai11
	 * @param jsonString
	 * @param entity
	 * @return
	 * @since JDK 1.8
	 */
	public static <T> boolean requestIsValid(String jsonString, Class<T> entity) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			objectMapper.readValue(jsonString, entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 
	 * strIsNotNullOrEmpty:(n个字符串都不是空). <br/>
	 * 
	 * @author lishuai11
	 * @param params
	 * @return
	 * @since JDK 1.8
	 */
	public static boolean strIsNotNullOrEmpty(String... params) {
		for (String param : params) {
			if (StringUtils.isBlank(param)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * birthdayIsValid:(生日). <br/>
	 * 
	 * @author lishuai11
	 * @param birthday
	 * @return
	 * @since JDK 1.8
	 */
	public static boolean birthdayIsValid(String birthday) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			simpleDateFormat.parse(birthday);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 
	 * phoneIsValid:(电话号). <br/>
	 * 
	 * @author lishuai11
	 * @param phone
	 * @return
	 * @since JDK 1.8
	 */
	public static boolean mobileIsValid(String mobile) {
		String check = "^(((13[0-9])|(14[579])|(15([0-3]|[5-9]))|(16[6])|(17[0135678])|(18[0-9])|(19[89]))\\d{8})$";
		Pattern regex = Pattern.compile(check);
		Matcher matcher = regex.matcher(mobile);
		return matcher.matches();
	}
	
	public static boolean passwordIsValid(String password) {
		String check = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[^]{8,16}$";
		Pattern regex = Pattern.compile(check);
		Matcher matcher = regex.matcher(password);
		return matcher.matches();
	}
	
	public static boolean addressMobileIsValid(String phone) {
		String check = "((^(((13[0-9])|(14[579])|(15([0-3]|[5-9]))|(16[6])|(17[0135678])|(18[0-9])|(19[89]))\\d{8})$))|(^(0\\d{2}-\\d{8}(-\\d{1,4})?)|(0\\d{3}-\\d{7,8}(-\\d{1,4})?)$)";
		Pattern regex = Pattern.compile(check);
		Matcher matcher = regex.matcher(phone);
		return matcher.matches();
	}

	public static boolean emailIsValid(String email) {
		String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
		Pattern regex = Pattern.compile(check);
		Matcher matcher = regex.matcher(email);
		return matcher.matches();
	}

	/**
	 * 
	 * idCardIsValid:(身份证). <br/>
	 * 
	 * @author lishuai11
	 * @param idCard
	 * @return
	 * @since JDK 1.8
	 */
	public static boolean idCardIsValid(String idCard) {
		String check = "^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$";
		Pattern regex = Pattern.compile(check);
		Matcher matcher = regex.matcher(idCard);
		return matcher.matches();
	}

}
