package com.magic.util;

/**
 * ClassName: CodeUtile <br/>
 * Function: 识别码. <br/>
 * date: 2018年11月1日 下午1:29:12 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public class CodeUtil {

	private static final String CODE_PRE = "MG_";

	public static String code(int id) {
		if (id < 10) {
			return CODE_PRE + "00000" + id;
		}
		if (id < 100) {
			return CODE_PRE + "0000" + id;
		}
		if (id < 1000) {
			return CODE_PRE + "000" + id;
		}
		if (id < 10000) {
			return CODE_PRE + "00" + id;
		}
		if (id < 100000) {
			return CODE_PRE + "0" + id;
		}
		if (id < 1000000) {
			return CODE_PRE + +id;
		}
		return null;
	}

}
