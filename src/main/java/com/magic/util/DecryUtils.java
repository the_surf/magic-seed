package com.magic.util;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * ClassName: DecryUtils <br/>
 * Function: 解密微信用户的电话号码. <br/>
 * date: 2018年8月31日 上午11:59:41 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
public class DecryUtils {

	public static byte[] decrypt(byte[] sessionkey, byte[] iv, byte[] encryptedData) throws Exception {
		AlgorithmParameterSpec ivSpec = new IvParameterSpec(iv);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		SecretKeySpec keySpec = new SecretKeySpec(sessionkey, "AES");
		cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
		return cipher.doFinal(encryptedData);
	}

}
