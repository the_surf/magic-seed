package com.magic.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.magic.result.Result;
import com.magic.service.LoginService;
import com.magic.util.ValidUtil;
import com.magic.vo.LoginCode;
import com.magic.vo.LoginPhone;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * ClassName: LoginController <br/>
 * Function: login. <br/>
 * date: 2018年10月16日 上午9:09:41 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Api(value = "登陆")
@RestController
@RequestMapping(value = "/system/login/")
public class LoginController {

	@Autowired
	LoginService loginService;

	/**
	 * 
	 * phoneLogin:(手机登陆). <br/> 
	 * 
	 * @author lishuai11 
	 * @param loginPhone
	 * @return 
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "手机登陆", httpMethod = "POST", notes = "手机号，密码登陆")
	@ApiImplicitParam(name = "loginPhone", value = "手机密码登陆请求体", required = true, dataType = "LoginPhone")
	@RequestMapping(value = "phone", method = RequestMethod.POST)
	public Result phoneLogin(@RequestBody LoginPhone loginPhone) {
		List<String> list = ValidUtil.validate(loginPhone);
		if (list != null && !list.isEmpty()) {
			StringBuilder errorString = new StringBuilder();
			list.forEach(row -> {
				errorString.append(row).append(";");
			});
			return Result.fail(Result.CLIENT_ERROR, errorString.toString());
		}
		return loginService.loginPhone(loginPhone);
	}

	/**
	 * 
	 * codeLogin:(验证码登陆). <br/>
	 * 
	 * @author lishuai11
	 * @param loginCode
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "手机验证码登陆", httpMethod = "POST", notes = "手机验证码登陆")
	@ApiImplicitParam(name = "loginCode", value = "手机验证码登陆请求体", required = true, dataType = "LoginCode")
	@RequestMapping(value = "code", method = RequestMethod.POST)
	public Result codeLogin(@RequestBody LoginCode loginCode) {
		List<String> list = ValidUtil.validate(loginCode);
		if (list != null && !list.isEmpty()) {
			StringBuilder errorString = new StringBuilder();
			list.forEach(row -> {
				errorString.append(row).append(";");
			});
			return Result.fail(Result.CLIENT_ERROR, errorString.toString());
		}
		return loginService.loginCode(loginCode);
	}

}
