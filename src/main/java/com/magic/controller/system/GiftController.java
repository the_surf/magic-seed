package com.magic.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magic.entity.gift.Gift;
import com.magic.result.Result;
import com.magic.service.gift.GiftService;
import com.magic.util.ValidUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * ClassName: GiftController <br/>
 * Function: 礼物中心. <br/>
 * date: 2018年10月16日 下午7:08:20 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Api(value = "礼物管理")
@RestController
@RequestMapping("/system/")
public class GiftController {

	@Autowired
	GiftService giftService;

	/**
	 * 
	 * giftList:(礼物列表). <br/>
	 * 
	 * @author lishuai11
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "礼物列表", httpMethod = "GET", notes = "获取礼物列表", response = Gift.class, responseContainer = "list")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "pageNum", value = "页码", required = false, dataType = "int", defaultValue = "0", paramType = "query"),
			@ApiImplicitParam(name = "pageSize", value = "页大小", required = false, dataType = "int", defaultValue = "10", paramType = "query") })
	@RequestMapping(value = "gifts", method = RequestMethod.GET)
	public Result giftList(@RequestParam(defaultValue = "0", required = false) int pageNum,
			@RequestParam(defaultValue = "10", required = false) int pageSize) {
		return giftService.giftList(pageNum, pageSize);
	}

	/**
	 * 
	 * giftList:(获取单个礼物). <br/>
	 * 
	 * @author lishuai11
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "获取单个礼物", httpMethod = "GET", notes = "根据礼物id查询礼物")
	@ApiImplicitParam(name = "giftId", value = "礼物id", required = true, dataType = "int", paramType = "path")
	@RequestMapping(value = "gift/{giftId}", method = RequestMethod.GET)
	public Result giftOne(@PathVariable(name = "giftId") int giftId) {
		return giftService.giftOne(giftId);
	}

	/**
	 * 
	 * deleteGift:(删除礼物). <br/>
	 * 
	 * @author lishuai11
	 * @param giftId
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "删除礼物", httpMethod = "DELETE", notes = "根据giftId删除礼物")
	@ApiImplicitParam(name = "giftId", value = "礼物id", required = true, dataType = "int", paramType = "path")
	@RequestMapping(value = "gift/{giftId}", method = RequestMethod.DELETE)
	public Result deleteGift(@PathVariable(name = "giftId") int giftId) {
		return giftService.deleteGift(giftId);
	}

	/**
	 * 
	 * addGift:(添加礼物). <br/>
	 * 
	 * @author lishuai11
	 * @param gift
	 * @param result
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "添加礼物", httpMethod = "POST", notes = "根据giftId删除礼物")
	@ApiImplicitParam(name = "gift", value = "礼物请求体", required = true, dataType = "Gift")
	@RequestMapping(value = "gift", method = RequestMethod.POST)
	public Result addGift(@RequestBody Gift gift) {
		List<String> list = ValidUtil.validate(gift);
		if (list != null && !list.isEmpty()) {
			StringBuilder errorString = new StringBuilder();
			list.forEach(row -> {
				errorString.append(row).append(";");
			});
			return Result.fail(Result.CLIENT_ERROR, errorString.toString());
		}
		return giftService.addGift(gift);
	}

	/**
	 * 
	 * updateGift:(更新礼物). <br/>
	 * 
	 * @author lishuai11
	 * @param gift
	 * @param result
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "修改礼物", httpMethod = "PUT", notes = "修改礼物")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "giftId", value = "礼物id", required = true, dataType = "int", paramType = "path"),
			@ApiImplicitParam(name = "gift", value = "礼物请求体", required = true, dataType = "Gift") })
	@RequestMapping(value = "gift/{giftId}", method = RequestMethod.PUT)
	public Result updateGift(@PathVariable(name = "giftId") int giftId, @RequestBody Gift gift) {
		List<String> list = ValidUtil.validate(gift);
		if (list != null && !list.isEmpty()) {
			StringBuilder errorString = new StringBuilder();
			list.forEach(row -> {
				errorString.append(row).append(";");
			});
			return Result.fail(Result.CLIENT_ERROR, errorString.toString());
		}
		return giftService.updateGift(giftId, gift);
	}

}
