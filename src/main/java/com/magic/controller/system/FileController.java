package com.magic.controller.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.magic.result.Result;
import com.magic.service.file.FileService;
import com.magic.vo.file.FileVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * ClassName: FileController <br/>
 * Function: 文件上传. <br/>
 * date: 2018年11月8日 下午3:45:47 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Api(value = "文件管理")
@RestController
@RequestMapping("/system/")
public class FileController {

	@Autowired
	FileService fileService;

	@ApiOperation(value = "文件上传", httpMethod = "POST", notes = "上传文件", response = FileVo.class)
	@ApiImplicitParam(name = "file", value = "文件", required = true, dataType = "MultipartFile", paramType = "query")
	@RequestMapping(value = "/file", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
	public Result postFile(
			@RequestParam(name = "file") @ApiParam(name = "file", value = "文件", required = true) MultipartFile file) {
		return fileService.upload(file);
	}

}
