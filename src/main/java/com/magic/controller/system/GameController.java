package com.magic.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magic.entity.game.Game;
import com.magic.result.Result;
import com.magic.service.game.GameService;
import com.magic.util.ValidUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * ClassName: GameController <br/>
 * Function: 游戏. <br/>
 * date: 2018年10月16日 上午9:10:05 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Api(value = "游戏模式")
@RestController
@RequestMapping("/system/")
public class GameController {

	@Autowired
	GameService gameService;

	@ApiOperation(value = "添加游戏", httpMethod = "POST", notes = "添加游戏模式", response = Game.class)
	@ApiImplicitParam(name = "game", value = "游戏请求体", required = true, dataType = "Game")
	@RequestMapping(value = "game", method = RequestMethod.POST)
	public Result postGame(@RequestBody Game game) {
		List<String> list = ValidUtil.validate(game);
		if (list != null && !list.isEmpty()) {
			StringBuilder errorString = new StringBuilder();
			list.forEach(row -> {
				errorString.append(row).append(";");
			});
			return Result.fail(Result.CLIENT_ERROR, errorString.toString());
		}
		return gameService.addGame(game);
	}

	@ApiOperation(value = "所有游戏", httpMethod = "GET", notes = "查询所有游戏模式", response = Game.class, responseContainer = "list")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "pageNum", value = "页码", required = false, dataType = "int", defaultValue = "0", paramType = "query"),
			@ApiImplicitParam(name = "pageSize", value = "页大小", required = false, dataType = "int", defaultValue = "10", paramType = "query") })
	@RequestMapping(value = "games", method = RequestMethod.GET)
	public Result getGames(@RequestParam(defaultValue = "0", required = false) int pageNum,
			@RequestParam(defaultValue = "10", required = false) int pageSize) {
		return gameService.getGames(pageNum, pageSize);
	}

	@ApiOperation(value = "单个游戏", httpMethod = "GET", notes = "查询单个游戏", response = Game.class)
	@ApiImplicitParam(name = "gameId", value = "游戏id", required = true, dataType = "int", paramType = "path")
	@RequestMapping(value = "game/{gameId}", method = RequestMethod.GET)
	public Result getGame(@PathVariable(name = "gameId") int gameId) {
		return gameService.getGame(gameId);
	}

	@ApiOperation(value = "游戏的种花记录", httpMethod = "GET", notes = "查询该场次游戏的种花记录")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "gameId", value = "游戏id（game_id）", required = true, dataType = "int", paramType = "path"),
			@ApiImplicitParam(name = "pageNum", value = "页码", required = false, dataType = "int", defaultValue = "0", paramType = "query"),
			@ApiImplicitParam(name = "pageSize", value = "页大小", required = false, dataType = "int", defaultValue = "10", paramType = "query") })
	@RequestMapping(value = "game/{gameId}/flower/hisotory", method = RequestMethod.GET)
	public Result gameFlowerHistory(@PathVariable(name = "gameId") int gameId,
			@RequestParam(defaultValue = "0", required = false) int pageNum,
			@RequestParam(defaultValue = "10", required = false) int pageSize) {
		return gameService.gameHistory(gameId, pageNum, pageSize);
	}

}
