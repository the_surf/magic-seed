package com.magic.controller.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.magic.result.Result;
import com.magic.service.RegisterService;
import com.magic.util.ValidUtil;
import com.magic.vo.ForgetPassword;
import com.magic.vo.MobileRegister;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * ClassName: RegisterController <br/>
 * Function: 注册. <br/>
 * date: 2018年10月16日 上午9:09:28 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Api(value = "注册")
@RestController
@RequestMapping("/system/register/")
public class RegisterController {

	@Autowired
	RegisterService registerService;

	/**
	 * 
	 * mobileRegister:(手机号注册). <br/>
	 * 
	 * @author lishuai11
	 * @param mobileRegister
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "手机号注册", httpMethod = "POST", notes = "手机号注册")
	@ApiImplicitParam(name = "mobileRegister", value = "手机号注册请求体", required = true, dataType = "MobileRegister")
	@RequestMapping(value = "mobile", method = RequestMethod.POST)
	public Result mobileRegister(@RequestBody MobileRegister mobileRegister) {
		List<String> list = ValidUtil.validate(mobileRegister);
		if (list != null && !list.isEmpty()) {
			StringBuilder errorString = new StringBuilder();
			list.forEach(row -> {
				errorString.append(row).append(";");
			});
			return Result.fail(Result.CLIENT_ERROR, errorString.toString());
		}
		return registerService.mobileRegister(mobileRegister);
	}

	/**
	 * 
	 * code:(获取验证码). <br/>
	 * 
	 * @author lishuai11
	 * @param mobile
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "获取验证码", httpMethod = "GET", notes = "获取验证码")
	@ApiImplicitParam(name = "mobile", value = "手机号", required = true, dataType = "string", paramType = "path")
	@RequestMapping(value = "code/{mobile}", method = RequestMethod.GET)
	public Result code(@PathVariable(name = "mobile") String mobile) {
        if (!ValidUtil.mobileIsValid(mobile)) {
			return Result.fail(Result.CLIENT_ERROR, "非法手机号");
		}
		return registerService.code(mobile);
	}

	/**
	 * 
	 * forgetPassword:(忘记密码). <br/>
	 * 
	 * @author lishuai11
	 * @return
	 * @since JDK 1.8
	 */
	@ApiOperation(value = "忘记密码", httpMethod = "POST", notes = "忘记密码")
	@ApiImplicitParam(name = "forgetPassword", value = "忘记密码请求体", required = true, dataType = "ForgetPassword")
	@RequestMapping(value = "forget", method = RequestMethod.POST)
	public Result forgetPassword(@RequestBody ForgetPassword forgetPassword) {
		List<String> list = ValidUtil.validate(forgetPassword);
		if (list != null && !list.isEmpty()) {
			StringBuilder errorString = new StringBuilder();
			list.forEach(row -> {
				errorString.append(row).append(";");
			});
			return Result.fail(Result.CLIENT_ERROR, errorString.toString());
		}
		if (!forgetPassword.getFirstPassword().equals(forgetPassword.getSecondPassword())) {
			return Result.fail(Result.CLIENT_ERROR, "前后密码输入不一致");
		}
		return registerService.forgetPassword(forgetPassword);
	}

}
