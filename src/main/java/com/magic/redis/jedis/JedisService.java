package com.magic.redis.jedis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * ClassName: JedisService <br/>
 * Function: jedis client 用于redis的操作. <br/>
 * date: 2018年08月23日 下午9:48:32 <br/>
 * 
 * @author lishuai8
 * @version
 * @since JDK 1.8
 */
@Service("jedisService")
public class JedisService {

	@Autowired
	JedisPool jedisPool;

	/**
	 * 
	 * get:(根据key值查value). <br/>
	 * 
	 * @author lishuai8
	 * @param key
	 * @return
	 * @since JDK 1.8
	 */
	public String get(String key) throws Exception {
		Jedis jedis = jedisPool.getResource();
		String str = jedis.get(key);
		close(jedis);
		return str;
	}

	/**
	 * 
	 * set:(设置key:value，没有失效时间). <br/>
	 * 
	 * @author lishuai8
	 * @param key
	 * @param value
	 * @return
	 * @since JDK 1.8
	 */
	public String set(String key, String value) throws Exception {
		Jedis jedis = jedisPool.getResource();
		String str = jedis.set(key, value);
		close(jedis);
		return str;
	}

	/**
	 * 
	 * setex:(设置key:value;并设置key的失效时间(单位s)). <br/>
	 * 如果key存在，则覆盖key的value；否则创建key:Lvalue
	 * 
	 * @author lishuai8
	 * @param key
	 * @param seconds
	 * @param value
	 * @return
	 * @since JDK 1.8
	 */
	public String setex(String key, int seconds, String value) throws Exception {
		Jedis jedis = jedisPool.getResource();
		String str = jedis.setex(key, seconds, value);
		close(jedis);
		return str;
	}

	/**
	 * 
	 * isExist:(查看key是否存在). <br/>
	 * 
	 * @author lishuai8
	 * @param key
	 * @return
	 * @since JDK 1.8
	 */
	public boolean isExist(String key) throws Exception {
		Jedis jedis = jedisPool.getResource();
		boolean result = jedis.exists(key);
		close(jedis);
		return result;
	}

	public void delete(String key) throws Exception {
		Jedis jedis = jedisPool.getResource();
		jedis.del(key);
		close(jedis);
	}
	
	public void lpush(String key,String jsonString) throws Exception {
		Jedis jedis = jedisPool.getResource();
		jedis.lpush(key, jsonString);
		close(jedis);
	}
	
	public String rpop(String key) throws Exception{
		Jedis jedis = jedisPool.getResource();
		String value = jedis.rpop(key);
		close(jedis);
		return value;
	}

	public void close(Jedis jedis) throws Exception {
		jedis.close();
	}

}
