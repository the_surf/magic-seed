package com.magic.service;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.magic.auth.TokenUtil;
import com.magic.entity.user.User;
import com.magic.redis.jedis.JedisService;
import com.magic.repository.IUserRepository;
import com.magic.result.Result;
import com.magic.vo.LoginCode;
import com.magic.vo.LoginPhone;

/**
 * ClassName: LoginService <br/>
 * Function: 登陆service. <br/>
 * date: 2018年10月16日 下午12:32:49 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Service("loginService")
public class LoginService {

	private static final Logger logger = LoggerFactory.getLogger(LoginService.class);

	@Autowired
	IUserRepository userRepository;

	@Autowired
	JedisService jedisService;

	/**
	 * 
	 * loginPhone:(手机登陆). <br/>
	 * 
	 * @author lishuai11
	 * @param loginPhone
	 * @return
	 * @since JDK 1.8
	 */
	public Result loginPhone(LoginPhone loginPhone) {
		try {
			User user = findByName(loginPhone.getMobile());
			if (user == null) {
				return Result.fail(Result.CLIENT_ERROR, "账号不存在");
			} else {
				if (user.getPassword().equals(DigestUtils.md5Hex(loginPhone.getPassword()))) {
					user.setDeviceCode(loginPhone.getDeviceCode());
					user = userRepository.save(user);
					JSONObject jsonObject = (JSONObject) JSONObject.toJSON(user);
					jsonObject.put("Authorization", TokenUtil.getToken(user.getUserId()));
					return Result.success("登陆成功", jsonObject);
				} else {
					return Result.fail(Result.CLIENT_ERROR, "密码错误");
				}
			}
		} catch (Exception e) {
			logger.error("LoginService.loginPhone()方法异常{}:{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}

	}

	/**
	 * 
	 * loginCode:(验证码登陆). <br/>
	 * 
	 * @author lishuai11
	 * @param loginCode
	 * @return
	 * @since JDK 1.8
	 */
	public Result loginCode(LoginCode loginCode) {
		try {
			if (!jedisService.isExist(loginCode.getMobile())) {
				return Result.fail(Result.CLIENT_ERROR, "验证码超时");
			}
			// ②：判断验证码是否正确
			String param = jedisService.get(loginCode.getMobile());
			if (!param.equals(loginCode.getCode())) {
				return Result.fail(Result.CLIENT_ERROR, "验证码错误");
			}
			User user = findByName(loginCode.getMobile());
			if (user == null) {
				return Result.fail(Result.CLIENT_ERROR, "用户不存在");
			} else {
				JSONObject jsonObject = (JSONObject) JSONObject.toJSON(user);
				jsonObject.put("Authorization", TokenUtil.getToken(user.getUserId()));
				return Result.success("登陆成功", jsonObject);
			}
		} catch (Exception e) {
			logger.error("LoginService.loginCode()方法异常{}:{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}

	}

	public Result loginWechat() {
		return null;
	}

	public User findByName(String userName) {
		List<User> users = userRepository.findByUserName(userName);
		if (users != null && !users.isEmpty()) {
			return users.get(0);
		}
		return null;
	}
}
