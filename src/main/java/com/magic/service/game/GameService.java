package com.magic.service.game;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magic.entity.game.Game;
import com.magic.repository.IGameHistoryRepository;
import com.magic.repository.IGameRepository;
import com.magic.result.Result;
import com.magic.service.user.UserService;
import com.magic.time.CountDown;

/**
 * ClassName: GameService <br/>
 * date: 2018年10月21日 下午3:53:40 <br/>
 * 
 * @author lishuai11
 * @version
 * @since JDK 1.8
 */
@Service("gameService")
public class GameService {

	@Autowired
	IGameRepository gameRepository;

	@Autowired
	IGameHistoryRepository gameHistoryRepository;

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	public Result addGame(Game game) {
		try {
			game = gameRepository.save(game);
			CountDown.addMapId(game);
			return Result.success("成功", gameRepository.save(game));
		} catch (Exception e) {
			logger.error("GameService.addGame()方法异常{}:{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

	public Result getGames(int pageNum, int pageSize) {
		try {
			pageNum = pageNum * pageSize;
			return Result.success("成功", gameRepository.pageSearchfindAll(pageNum, pageSize));
		} catch (Exception e) {
			logger.error("GameService.getGames()方法异常{}:{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

	public Result getGame(int gameId) {
		try {
			return Result.success("成功", gameRepository.findOne(gameId));
		} catch (Exception e) {
			logger.error("GameService.getGames()方法异常{}:{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

	public Result gameHistory(int gameId, int pageNum, int pageSize) {
		try {
			pageNum = pageNum * pageSize;
			return Result.success("成功", gameHistoryRepository.pageSearchGameHistory(pageNum, pageSize, gameId));
		} catch (Exception e) {
			logger.error("GameService.gameHistory()方法异常{}:{}", e.getMessage(), e);
			return Result.fail(Result.SERVER_ERROR, "服务器异常");
		}
	}

	public List<Game> findAll() {
		return (List<Game>) gameRepository.findAll();
	}
}
